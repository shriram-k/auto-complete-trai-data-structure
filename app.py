from flask import Flask, request, jsonify, render_template
from flask_cors import CORS
from setup_data import get_trie_object
app = Flask(__name__)
cors = CORS(app)
trie = get_trie_object()


@app.route("/", methods=['GET'])
def get_page():
    return render_template("index.html")


@app.route("/search", methods=['GET'])
def auto_complete():
    keyword = request.args.get("keyword")
    suggestions = trie.start_with_prefix(keyword)

    return jsonify({"suggestions": suggestions})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=4201, debug=True)



