class Node:
    def __init__(self, letter=None, word=None):
        self.letter = letter
        self.word = word
        self.children = {}

    def add_new_child(self, parent_letter, word=None):
        if not isinstance(parent_letter, Node):
            self.children[parent_letter] = Node(parent_letter, word)
        else:
            self.children[parent_letter.letter] = parent_letter

    def __getitem__(self, item):
        return self.children[item]


class Trie:
    def __init__(self):
        self.head = Node()

    def __getitem__(self, item):
        return self.head.children[item]

    def add_word(self, word):
        current_node = self.head
        is_word_complete = True

        for i in range(len(word)):
            if word[i] in current_node.children:
                current_node = current_node.children[word[i]]
            else:
                is_word_complete = False
                break

        if not is_word_complete:
            while i < len(word):
                current_node.add_new_child(word[i])
                current_node = current_node.children[word[i]]
                i += 1

        current_node.word = word

    def has_word(self, word):
        if word == '':
            return False

        if word is None:
            return False

        current_node = self.head
        node_exists = True

        for letter in word:
            if letter in current_node.children:
                current_node = current_node.children[letter]
            else:
                node_exists = False
                break

        if node_exists:
            if current_node.word is None:
                node_exists = False

        return node_exists

    def start_with_prefix(self, prefix):
        words = []
        if prefix is None:
            return words

        top_node = self.head
        for letter in prefix:
            if letter in top_node.children:
                top_node = top_node.children[letter]
            else:
                return words

        if top_node == self.head:
            queue = [node for key, node in top_node.children.items()]
        else:
            queue = [top_node]

        while queue:
            current_node = queue.pop()
            if current_node.word is not None:
                words.append(current_node.word)

            queue = [node for key, node in current_node.children.items()] + queue

        return words

    def get_data(self, word):
        if not self.has_word(word):
            return []

        current_node = self.head
        for letter in word:
            current_node = current_node[letter]

        return current_node.word


def get_trie_object():
    trie = Trie()
    with open("./movies.txt", "r") as raw_text:
        data = raw_text.read()
        for each_line in data.splitlines():
            for each_word in each_line.split():
                trie.add_word(each_word)

    return trie



